import os
import numpy as np
import tensorflow as tf
from object_detection.utils import label_map_util

# Try using GPUs if available
gpus = tf.config.experimental.list_physical_devices("GPU")
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

class AiRunner:
    """ Analyses images and provides methods to aquire key measures
    
    Attributes:
        Model: The frozen model to be used for analysis
        ModelFn: The model's function used for analysis
        CategoryIndex: Representation of the label map
        DetectionThreshold: Percentage above which a box is accepted
        NumOfDetections: Number of usable boxes found in the latest analysis
    """
    def __init__(self, threshold: float):
        """Instantiates a new AiRunner instance
        
        threshold:
            Detection threshold in % to decide from what percentage a detection is valid
            Should be between 0 and 100, with 0% accepting every box as a detection and 100% accepting only perfect matches (which is very unlikely)"""
        self.DetectionThreshold = threshold

    def init(self, model_path: str):
        """Initialises the saved model and corresponding variables
        
        model_path:
            Path to the saved model. This directory has to contain both the saved model (usually saved_model.pb) and label map (usually label_map.pbtxt)"""
        if(not os.path.exists(model_path)):
            model_path = "saved_model"
        if(not os.path.exists(model_path)):
            print("Model not found, please enter path to saved_model.")
            return False
        self.Model = tf.saved_model.load(model_path)
        self.ModelFn = self.Model.signatures['serving_default']
        PATH_TO_LABELS = os.path.join(model_path, "label_map.pbtxt")
        if(not os.path.exists(PATH_TO_LABELS)):
            print("Please place the label_map.pbtxt in the saved_model directory")
            return False
        self.CategoryIndex = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)
        return True

    def analyse(self, image):
        """Analyses an image to provide data. 
        Afterwards, results can be obtained via the other functions:
            found, get_bearing, estimate_distance, get_best_match
        They will provide data obtained from the latest analysis."""
        # Prepare image
        bitmap = np.asarray(image)
        input = tf.convert_to_tensor(bitmap)[tf.newaxis, ...]

        # Run inference
        output = self.ModelFn(input)

        # All outputs are batches tensors.
        # Convert to numpy arrays, and take index [0] to remove the batch dimension.
        # We're only interested in the first num_detections.
        num_detections = int(output.pop('num_detections'))
        output = {key:value[0, :num_detections].numpy() 
                        for key,value in output.items()}
        
        output['detection_classes'] = output['detection_classes'].astype(np.int64)
        self.LastOutput = output
        self.BestMatch = output['detection_boxes'][0]
        self.NumOfDetections = 0
        while(output['detection_scores'][self.NumOfDetections]*100 >= self.DetectionThreshold):
            self.NumOfDetections = self.NumOfDetections +1

    def found(self):
        """Whether or not at least one box was detected"""
        return self.NumOfDetections > 0

    def get_bearing(self):
        """Returns a value between -1 and +1 to indicate the position of the best match within the last analysed image relative to its middle."""
        left_border = self.BestMatch[1]
        right_border = self.BestMatch[3]
        mid_of_box = left_border + (right_border-left_border)/2
        bearing = (mid_of_box - 0.5)*2 # double it so instead of [-0.5, 0.5] we get [-1, 1]
        return bearing

    def estimate_distance(self):
        """Returns a guess about how far await the detected box is.
        Unit depends on the field of view of the image.
        
        The calculation goes 1/(width*height) of the detected box, with both values between 0 and 1 according to
            what part of the image the box covers.
        If the whole image is detected, this will return 1.
        If a vertical box with 50% width is detected, this will """
        upper_border = self.BestMatch[0]
        left_border = self.BestMatch[1]
        lower_border = self.BestMatch[2]
        right_border = self.BestMatch[3]
        width = right_border - left_border
        height = lower_border - upper_border
        return 1/(width*height)

    def get_best_match(self):
        """Returns an array representing the box of the best match of the latest analysis.
        The array looks like [y1,x1,y2,x2] and contains floats between 0 and 1.
        [0] is the upper bound
        [1] is the left bound
        [2] is the lower bound
        [3] is the right bound"""
        return self.BestMatch