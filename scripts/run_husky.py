#!/usr/bin/env python

# %% Import modules
import sys
import numpy as np
import wrapper as husky
from ai_runner import AiRunner

# %% Initialise ai runner, loading the model and required data
ai = AiRunner(50)
if(not ai.init(sys.argv[1] if len(sys.argv) > 1 else "_")):
    exit(1)

# %% Thresholds 
# Possibly make configurable through ROS later
# In M because that's what the depth calculation returns
MIN_TURN_DISTANCE = 0.1
MIN_TURN_VALUE = 0
MIN_APPROACH_DISTANCE = 0.1


# %% helper functions
def get_distance(depth: np.ndarray):
    """Returns the distance in meters from the camera to the detected object"""
    box = ai.get_best_match()
    height = depth.shape[0]
    width = depth.shape[1]
    # box has [y1,x1,y2,x2]
    intBox = np.array([box[0]*height, box[1]*width, box[2]*height, box[3]*width]).astype(int)
    #print("intBox:", intBox)
    # depth calculation only for the part within the bounding box
    cv_image = depth[intBox[0]:intBox[2], intBox[1]:intBox[3]]
    #print("cv_image shape:", cv_image.shape)
    return cv_image[cv_image > 0].min() / 1000

# %% main function, callback from cam to take action on an image
def on_image_received(colour, depth):
    ai.analyse(colour)
    if(ai.found()):
        distance = get_distance(np.asanyarray(depth))
        if(distance > MIN_TURN_DISTANCE):
            bearing = ai.get_bearing()
            if(abs(bearing) > MIN_TURN_VALUE or abs(distance)-1 > 3*MIN_TURN_DISTANCE):
                husky.turn(bearing)
            if(abs(distance)-1 > MIN_APPROACH_DISTANCE): # distance must be far enough away from 1m
                husky.go(distance)
            else: # Detection not far enough away to chase, just turn in place
                husky.go(1)
        else: # Detection too close, no movement
            husky.stop_moving()
    else: # No detection, no movement
        husky.stop_moving()

# %% Execute
husky.run_with_callback(on_image_received)
