# %% Imports
import cv2
import math
import numpy as np
import rospy
from sensor_msgs.msg import Image
from geometry_msgs.msg import Twist
from cv_bridge import CvBridge
from typing import Callable

# %% rostopics
TOPIC_CAM_COLOR_RAW = "/camera/color/image_raw"
TOPIC_CAM_DEPTH_RAW = "/camera/aligned_depth_to_color/image_raw"
TOPIC_CMD_VEL = "/cmd_vel"

# %% Local
#cap = cv2.VideoCapture(0)

# %% ROS
def callback_depth(image: Image):
    """Saves a depth image"""
    global depth
    depth = image

def callback_colour(image: Image):
    """Sends the given image and the saved depth image to the callback function"""
    global depth
    global callback
    callback(bridge.imgmsg_to_cv2(image, image.encoding), get_depth())

twistMsg = Twist()

pub = rospy.Publisher(TOPIC_CMD_VEL, Twist, queue_size=1)
depth_sub = rospy.Subscriber(TOPIC_CAM_DEPTH_RAW, Image, callback_depth, queue_size=1)

# %% Functions

bridge = CvBridge()
def get_depth() -> Image:
    """Returns the latest depth image"""
    return bridge.imgmsg_to_cv2(depth, depth.encoding)

RADIANS_PER_CIRCLE = 2*math.pi
turnspeed_mod = 1/RADIANS_PER_CIRCLE # connect to rostopic for dynamic angular speed control
def turn(bearing: float):
    """Sets the turn rate.
    
    bearing:
        Value between -1 for 100% turn speed counter-clockwise and 1 for 100% turn speed clockwise"""

    # angular uses radians per second, so angular.z = 2*PI would be complete 360° turns per second
    turn =-1*bearing*RADIANS_PER_CIRCLE*turnspeed_mod
    twistMsg.angular.z = turn
    pub.publish(twistMsg)
    print(twistMsg)

hold_distance = 1 # connect to rostopic for dynamic modification of how far from the target the robot should keep itself
linearspeed_mod = 1 # connect to rostopic for dynamic linear speed control
def go(dist: float):
    # linear uses m/s so linear x = 1 means move forward 1 meter per second
    twistMsg.linear.x = math.log((1-hold_distance)+dist)*linearspeed_mod
    pub.publish(twistMsg)
    print(twistMsg)

def stop_moving():
    """Stops the robot"""
    twistMsg.linear.x = 0
    twistMsg.angular.z = 0
    pub.publish(twistMsg)
    print(twistMsg)

def running():
    """Returns true if the node is still running"""
    return not rospy.is_shutdown()

# %% Startup functions, choose one

def run_with_callback(colour_depth_callback: Callable[[Image, Image], None]):
    """Starts the subscriber to listen to image messages from the realsense cam.
    
    colour_depth_callback:
        Is called when a new image is available.
        Gets both colour and depth image as parameters."""
    # ggf Thread starten
    global callback
    callback  = colour_depth_callback
    colour_sub = rospy.Subscriber(TOPIC_CAM_COLOR_RAW, Image, callback_colour, queue_size=1)
    rospy.init_node("marlons_person_follower")
    rospy.spin()

def run_local_with_callback(colour_depth_callback: Callable[[Image, Image], None]):
    """Starts the subscriber to listen to image messages from the realsense cam directly rather than through ROS."""
    import pyrealsense2 as rs
    global callback
    callback  = colour_depth_callback
    pipeline = rs.pipeline()
    config = rs.config()
    #device_product_line = str(config.resolve(rs.pipeline_wrapper(pipeline)).get_device().get_info(rs.camera_info.product_line))
    config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 30)
    config.enable_all_streams()
    align = rs.align(rs.stream.color)
    pipeline.start(config)
    rospy.init_node("marlons_person_follower")
    rate = rospy.Rate(10)
    while(running()):
        frames = pipeline.wait_for_frames()
        frames = align.process(frames)
        colour_frame = frames.get_color_frame()
        colour_data = colour_frame.get_data()
        depth_frame = frames.get_depth_frame()
        depth_data = depth_frame.get_data()
        callback(colour_data, depth_data)
        rate.sleep()
